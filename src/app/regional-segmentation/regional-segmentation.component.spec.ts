import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionalSegmentationComponent } from './regional-segmentation.component';

describe('RegionalSegmentationComponent', () => {
  let component: RegionalSegmentationComponent;
  let fixture: ComponentFixture<RegionalSegmentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionalSegmentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionalSegmentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
