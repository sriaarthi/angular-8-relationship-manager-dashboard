import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcosystemPartnersComponent } from './ecosystem-partners.component';

describe('EcosystemPartnersComponent', () => {
  let component: EcosystemPartnersComponent;
  let fixture: ComponentFixture<EcosystemPartnersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcosystemPartnersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcosystemPartnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
