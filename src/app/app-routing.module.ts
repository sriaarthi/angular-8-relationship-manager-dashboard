import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component'
import { HomeComponent} from './home/home.component'
import { RegionalSegmentationComponent } from './regional-segmentation/regional-segmentation.component'
import { ClientLocationComponent } from './client-location/client-location.component'
import { ClientCashFlowComponent } from './client-cash-flow/client-cash-flow.component'
import { PaymentAnomalyComponent } from './payment-anomaly/payment-anomaly.component'
import { EcosystemPartnersComponent } from './ecosystem-partners/ecosystem-partners.component'
import { PredictedAnalysisComponent } from './predicted-analysis/predicted-analysis.component'
 
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'admin'},
  { path: 'login', component: LoginComponent },
  { path: 'admin', component: AdminComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'regional-segmentation',
        component: RegionalSegmentationComponent
      },
      {
        path: 'client-location',
        component: ClientLocationComponent
      },
      {
        path: 'client-cashflow',
        component: ClientCashFlowComponent
      },
      {
        path: 'payment-anomaly',
        component: PaymentAnomalyComponent
      },
      {
        path: 'ecosystem-partners',
        component: EcosystemPartnersComponent
      },
      {
        path: 'predicted-analysis',
        component: PredictedAnalysisComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
