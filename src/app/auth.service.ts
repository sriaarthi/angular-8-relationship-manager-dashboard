import { User } from './user';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from  '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //private REST_API_SERVER = "http://3.6.10.74/api/3.3/sites/c745d8c0-e514-4728-bde6-1174cf84d2a5/users";

  private SERVER_URL="http://3.6.10.74/api/3.3/"

  customersObservable : Observable<AuthService>;

  constructor(private  httpClient:HttpClient, private router: Router) { }

  public login(userInfo: User){
    //console.log("email::: " + userInfo.email + "pwd::: " + userInfo.password);

    localStorage.setItem('ACCESS_TOKEN', "access_token");
    this.sendGetRequest(userInfo);
  }

  public isLoggedIn(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;

  }

  public logout(){
    localStorage.removeItem('ACCESS_TOKEN');
  }

  public sendGetRequest(userInfo){

    console.log("inside fun......");
    console.log("un:--- " + userInfo.email);
    console.log("pwdd:--- " + userInfo.password);

      let postData = {
        "credentials": {
          "name": userInfo.email,
          "password": userInfo.password,
          "site": {
            "contentUrl": "RM"
          }
        }
      }

 
      this.httpClient.post("http://3.6.10.74/api/3.3/auth/signin",postData)
      .subscribe(
          data  => {
          console.log("POST Request is successful ", JSON.stringify(data));
          // console.log("tokeennllll--->> " + data.credentials.token);
          //this.getUserList(data);
          this.getTicket(userInfo,data);
        //  localStorage.setItem('Site_id',);
          this.router.navigateByUrl('/admin');
          },
          error  => {
            console.log("Error", error);
          }

      );


    console.log("iside sendGetRequest.........");
    
    

  //   const headers = new HttpHeaders({'X-Tableau-Auth':'45hQQ7vfS8etel0Vcya1KA|QoxyUSmDa9h4mWD6mqIpagDJ4aMPiJpD'});
    
  //   this.httpClient.get("http://3.6.10.74/api/3.3/sites/c745d8c0-e514-4728-bde6-1174cf84d2a5/users",{headers}).subscribe((data) => {
  //     console.log("dataa--->>> " + data);
  //     console.log(headers);
  //    console.log("data JSON--->> " + JSON.stringify(data));
  // });

  }

  public getTicket(userInfo,data){

    

    console.log("getticketttt--->>>> " + JSON.stringify(data));
    
    var token=data.credentials.token;
      var siteid=data.credentials.site.id;
      console.log("siteid:: " + siteid);
      localStorage.setItem('Site_id',siteid);
      localStorage.setItem('TOKEN',token);
   

    var params1 = new HttpParams();
    params1 = params1.set('username',userInfo.email);
    params1 = params1.set('target_site', "RM");
    //console.log()
    // var param1="username=RMUser&target_site=4d55ef3f-0365-489d-a16e-e6c34aeda481";
    const headers1 = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});

    let param2= new HttpParams().set('username',userInfo.email).set('target_site', "RM");
    console.log("param22--->> " + params1);
    this.httpClient.post("http://3.6.10.74/trusted",{param2})
      .subscribe(
          data  => {
          console.log("POST Request is successful ", JSON.stringify(data));
          var tkt=data;
          console.log("tttt-->> " + tkt);
          // console.log("tokeennllll--->> " + data.credentials.token);
          //this.getUserList(data);
         // this.router.navigateByUrl('/admin');
          },
          error  => {
            console.log("Error", error);
          }

      );

  }

  public getUserList(data:any){
      console.log("inside getUserList----->>>> ");
      //console.log("ttt:====" + data.credentials.token);
      var token=data.credentials.token;
      var siteid=data.credentials.site.id;

    const headers = new HttpHeaders({'X-Tableau-Auth':token});
    
     this.httpClient.get(this.SERVER_URL+"sites/"+siteid+"/users",{headers}).subscribe((res) => {
       console.log("resssss--->>> " + res);
       console.log(headers);
      console.log("data JSON--->> " + JSON.stringify(res));
      this.getUserObject(res);
     

   });

  }

  public getUserObject(data:any){
    console.log("getUserObject--->>>> " + JSON.stringify(data));
    var userObj = data.users.user.map(function(data) {
      console.log("name: " + data.name);
      return Number(data.id)
    });

  }

 
}
