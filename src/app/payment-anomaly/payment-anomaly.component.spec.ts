import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentAnomalyComponent } from './payment-anomaly.component';

describe('PaymentAnomalyComponent', () => {
  let component: PaymentAnomalyComponent;
  let fixture: ComponentFixture<PaymentAnomalyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentAnomalyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentAnomalyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
