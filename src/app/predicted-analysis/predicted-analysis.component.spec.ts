import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictedAnalysisComponent } from './predicted-analysis.component';

describe('PredictedAnalysisComponent', () => {
  let component: PredictedAnalysisComponent;
  let fixture: ComponentFixture<PredictedAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictedAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictedAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
