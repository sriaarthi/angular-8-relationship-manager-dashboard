import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgMagicIframeModule } from '@sebgroup/ng-magic-iframe';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatMenuModule } from  '@angular/material';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { ClientLocationComponent } from './client-location/client-location.component';
import { RegionalSegmentationComponent } from './regional-segmentation/regional-segmentation.component';
import { ClientCashFlowComponent } from './client-cash-flow/client-cash-flow.component';
import { PaymentAnomalyComponent } from './payment-anomaly/payment-anomaly.component';
import { EcosystemPartnersComponent } from './ecosystem-partners/ecosystem-partners.component';
import { PredictedAnalysisComponent } from './predicted-analysis/predicted-analysis.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    DashboardComponent,
    HomeComponent,
    RegionalSegmentationComponent,
    ClientLocationComponent,
    ClientCashFlowComponent,
    PaymentAnomalyComponent,
    EcosystemPartnersComponent,
    PredictedAnalysisComponent
  ],
  imports: [
    BrowserModule,
    NgMagicIframeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,  
    AppRoutingModule, BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
