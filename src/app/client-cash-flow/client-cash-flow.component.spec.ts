import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientCashFlowComponent } from './client-cash-flow.component';

describe('ClientCashFlowComponent', () => {
  let component: ClientCashFlowComponent;
  let fixture: ComponentFixture<ClientCashFlowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientCashFlowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientCashFlowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
