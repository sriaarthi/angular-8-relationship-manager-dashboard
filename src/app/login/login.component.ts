import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from  '@angular/router';
import { User } from  '../user';
import { AuthService } from  '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) { }

  loginForm: FormGroup;
  isSubmitted  =  false;

  ngOnInit() {
    //this.router.navigateByUrl('/https://login.microsoftonline.com/');
    this.loginForm  =  this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

  //   this.authService.sendGetRequest().subscribe((data: any[])=>{
  //     console.log(data);
  //    // this.products = data;
  // })

}

  get formControls() { return this.loginForm.controls; }


  login(){
    // window.open("https://login.microsoftonline.com/");
    console.log(this.loginForm.value);
    this.isSubmitted = true;
    // if(this.loginForm.invalid){
    //   return;
    // }
     this.authService.login(this.loginForm.value);
     //this.router.navigateByUrl('/admin');
  }

  // login(){
  //   console.log(this.loginForm.value);
  //   var data = this.loginForm.value;
  //   var email = data.email;
  //   var pwd = data.password;

  //   if(email=="RM102" && pwd=="123"){
  //       console.log("insideeeeee");
  //       this.isSubmitted = true;
  //       this.authService.login(this.loginForm.value);
  //       this.router.navigateByUrl('/admin');
  //   }
  //   else {
  //     this.isSubmitted = false;
  //     alert("invalid user....");
  //   }
  //   //this.isSubmitted = true;
  //   if(this.loginForm.invalid){
  //     return;
  //   }
    
  // }

}
