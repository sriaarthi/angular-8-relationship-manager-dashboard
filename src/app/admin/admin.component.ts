import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  siteId = localStorage.getItem('Site_id');
  token = localStorage.getItem('TOKEN');
  // url="http://3.6.10.74/trusted/"+this.token+"/t/RM/views/RM20190108/1?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no&:origin=viz_share_link";
  // console.log();

  ngOnInit() {
    console.log("inside onsubmit admin");
    console.log("ttttttt " + localStorage.getItem('Site_id'));
    console.log("siteId.... " + this.siteId);
  }

  logout(){
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

}
